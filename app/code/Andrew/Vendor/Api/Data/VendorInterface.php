<?php

namespace Andrew\Vendor\Api\Data;


interface VendorInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const VENDOR_ID                    = 'vendor_id';
    const NAME                         = 'name';
    const DESCRIPTION                  = 'description';
    const DATE_ADDED                   = 'date_added';
    const LOGO                         = 'logo';



    /**
     * Get ID
     *
     * @return int
     */
    public function getId();

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Get date_added
     *
     * @return string
     */
    public function getDate_added();

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setId($id);

    /**
     * Set name
     *
     * @param string $name
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setName($name);

    /**
     * Set description
     *
     * @param string $description
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setDescription($description);

    /**
     * Set date_added
     *
     * @param string $date_added
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setDate_added($date_added);

    /**
     * Set logo
     *
     * @param string $logo
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setLogo($logo);
}