<?php

namespace Andrew\Vendor\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 * @api
 * @since 100.0.2
 */
interface VendorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \Andrew\Vendor\Api\Data\VendorInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \Andrew\Vendor\Api\Data\VendorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
