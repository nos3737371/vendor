<?php

namespace Andrew\Vendor\Api;

interface VendorRepositoryInterface
{
    /**
     * Save vendor.
     *
     * @param \Andrew\Vendor\Api\Data\VendorInterface $vendor
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Andrew\Vendor\Api\Data\VendorInterface $vendor);

    /**
     * Retrieve vendor.
     *
     * @param int $vendorId
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($vendorId);

    /**
     * Retrieve vendors matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Andrew\Vendor\Api\Data\VendorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete vendor.
     *
     * @param \Andrew\Vendor\Api\Data\VendorInterface $vendor
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Andrew\Vendor\Api\Data\VendorInterface $vendor);

    /**
     * Delete vendor by ID.
     *
     * @param int $vendorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($vendorId);
}