<?php

namespace Andrew\Vendor\Block;

use Andrew\Vendor\Api\VendorRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;

class VendorAttributeList extends ListProduct
{
    /**
     * @var VendorRepositoryInterface
     */
    protected $_vendorFactory;

    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        VendorRepositoryInterface $vendorFactory,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );

        $this->_vendorFactory = $vendorFactory;
    }

    public function getVendorsData($productId)
    {
        $vendorsData = [];

        $i = 0;
        foreach ($this->getVendorIds($productId) as $id){
            $vendorsData[$i]['name'] = $this->_vendorFactory->getById($id)->getName();
            $vendorsData[$i]['description'] = $this->_vendorFactory->getById($id)->getDescription();
            $i++;
        }

        return $vendorsData;
    }

    private function getVendorIds($productId)
    {
        $ids = '';
        $objectManager = ObjectManager::getInstance();

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $objectManager->create('Magento\Catalog\Model\Product');
        $product->load($productId);
        $ids = $product->getData('vendor_attribute');

        return explode(',', $ids);
    }
}