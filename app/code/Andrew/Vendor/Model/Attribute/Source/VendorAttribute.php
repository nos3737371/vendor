<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Andrew\Vendor\Model\Attribute\Source;

class VendorAttribute extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * Vendor collection factory
     *
     * @var \Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory
     */
    protected $_vendorCollectionFactory;

    /**
     * Construct
     *
     * @param \Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory $vendorCollectionFactory
     */
    public function __construct(
        \Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory $vendorCollectionFactory
    )
    {
        $this->_vendorCollectionFactory = $vendorCollectionFactory;
    }

    /**
     * Return option array
     *
     * @param bool $addEmpty
     * @return array
     */
    public function getAllOptions($addEmpty = true)
    {
        /** @var \Andrew\Vendor\Model\ResourceModel\Vendor\Collection $collection */
        $collection = $this->_vendorCollectionFactory->create();

        $collection->getData();

        $options = [];

        if ($addEmpty) {
            $options[] = ['label' => __('-- Please Select a Vendor --'), 'value' => ''];
        }
        foreach ($collection as $vendor) {
            $options[] = ['label' => $vendor->getName(), 'value' => $vendor->getId()];
        }

        return $options;
    }
}

    /*
     /**
        * Get all options
        * @return array
        */
    //public function getAllOptions()
    //{
        /*if (!$this->_options) {
            $this->_options = [
                ['label' => __('Cotton'), 'value' => 'cotton'],
                ['label' => __('Leather'), 'value' => 'leather'],
                ['label' => __('Silk'), 'value' => 'silk'],
                ['label' => __('Denim'), 'value' => 'denim'],
                ['label' => __('Fur'), 'value' => 'fur'],
                ['label' => __('Wool'), 'value' => 'wool'],
                ['label' => __('Bitches'), 'value' => 'Suka'],
            ];
        }

        return $this->_options;
    }
}
*/