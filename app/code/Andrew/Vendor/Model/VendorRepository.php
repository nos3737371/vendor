<?php

namespace Andrew\Vendor\Model;

use Andrew\Vendor\Api\Data;
use Andrew\Vendor\Api\VendorRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Andrew\Vendor\Model\ResourceModel\Vendor as ResourceVendor;
use Andrew\Vendor\Model\VendorFactory as VendorFactory;
use Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;

class VendorRepository implements VendorRepositoryInterface
{
    protected $resource;
    protected $vendorFactory;
    protected $vendorCollectionFactory;
    protected $collectionProcessor;
    protected $searchResultsFactory;

    public function __construct(
        ResourceVendor $resource,
        VendorFactory $vendorFactory,
        VendorCollectionFactory $vendorCollectionFactory,
        Data\VendorSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor = null
    )
    {
        $this->resource = $resource;
        $this->vendorFactory = $vendorFactory;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }


    /**
     * Save Vendor.
     *
     * @param \Andrew\Vendor\Api\Data\VendorInterface $vendor
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Andrew\Vendor\Api\Data\VendorInterface $vendor)
    {
        try {
            $this->resource->save($vendor);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the page: %1', $exception->getMessage()),
                $exception
            );
        }
        return $vendor;
    }

    /**
     * Retrieve vendor.
     *
     * @param int $vendorId
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($vendorId)
    {
        $vendor = $this->vendorFactory->create();
        $vendor->load($vendorId);
        if (!$vendor->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $vendorId));
        }
        return $vendor;
    }

    /**
     * Retrieve vendors matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Andrew\Vendor\Api\Data\VendorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->vendorCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\VendorSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete vendor.
     *
     * @param \Andrew\Vendor\Api\Data\VendorInterface $vendor
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Andrew\Vendor\Api\Data\VendorInterface $vendor)
    {
        try {
            $this->resource->delete($vendor);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the page: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete Stpre by given Vendor Identity
     *
     * @param string $vendorId
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteById($vendorId)
    {
        return $this->delete($this->getById($vendorId));
    }
}