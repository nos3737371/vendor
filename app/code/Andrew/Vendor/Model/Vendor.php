<?php

namespace Andrew\Vendor\Model;

use Andrew\Vendor\Api\Data\VendorInterface;
use Magento\Framework\Model\AbstractModel;


class Vendor extends AbstractModel implements VendorInterface
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'andrew_vendor';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Andrew\Vendor\Model\ResourceModel\Vendor::class);
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::VENDOR_ID);
    }

    /**
     * Get NAME
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get Date_added
     *
     * @return string
     */
    public function getDate_added()
    {
        return $this->getData(self::DATE_ADDED);
    }


    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->getData(self::LOGO);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::VENDOR_ID, $id);
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Set Date_Added
     *
     * @param string $date_added
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setDate_added($date_added)
    {
        return $this->setData(self::DATE_ADDED, $date_added);
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return \Andrew\Vendor\Api\Data\VendorInterface
     */
    public function setLogo($logo)
    {
        return $this->setData(self::LOGO, $logo);
    }
}
