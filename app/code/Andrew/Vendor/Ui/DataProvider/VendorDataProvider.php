<?php

namespace Andrew\Vendor\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Andrew\Vendor\Model\Vendor as VendorModel;
use Andrew\Vendor\Model\ResourceModel\Vendor\Collection;
use Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory;

class VendorDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var VendorModel $vendor */
        foreach ($items as $vendor) {
            $this->loadedData[$vendor->getId()] = $vendor->getData();
        }

        $data = $this->dataPersistor->get('vendor');
        if (!empty($data)) {
            $vendor = $this->collection->getNewEmptyItem();
            $vendor->setData($data);
            $this->loadedData[$vendor->getId()] = $vendor->getData();
            $this->dataPersistor->clear('vendor');
        }

        return $this->loadedData;
    }
}