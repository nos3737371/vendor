<?php

namespace Andrew\Vendor\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Andrew\Vendor\Api\VendorRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Andrew_Vendor::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param VendorRepositoryInterface $vendorRepository
     */
    public function __construct(
        Context $context,
        VendorRepositoryInterface $vendorRepository
    ) {
        parent::__construct($context);
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('vendor_id');
        if ($id) {
            $vendor = $this->vendorRepository->getById($id);
            try {
                $this->vendorRepository->delete($vendor);
                $this->messageManager->addSuccess(__("Vendor was deleted success."));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Couldn't deleted the vendor."));
            }
        } else {
            $this->messageManager->addErrorMessage(__("Can't find the vendor."));
        }
        $this->_redirect("vendor/vendor/index");
    }
}
