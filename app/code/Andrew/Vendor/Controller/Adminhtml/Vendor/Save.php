<?php

namespace Andrew\Vendor\Controller\Adminhtml\Vendor;

use Andrew\Vendor\Api\Data\VendorInterfaceFactory;
use Andrew\Vendor\Api\VendorRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Andrew\Vendor\Api\Data\VendorInterface;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Andrew_Vendor::all";

    /**
     * @var VendorInterfaceFactory
     */
    private $vendorFactory;

    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param VendorInterfaceFactory $vendorFactory
     * @param VendorRepositoryInterface $vendorRepository
     */
    public function __construct(
        Context $context,
        VendorInterfaceFactory $vendorFactory,
        VendorRepositoryInterface $vendorRepository
    ) {
        parent::__construct($context);
        $this->vendorFactory = $vendorFactory;
        $this->vendorRepository = $vendorRepository;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var VendorInterface $vendor */
        $vendor = $this->vendorFactory->create();
        if (!empty($data['vendor_id']))
            $vendor->setId($data['vendor_id']);
        $vendor
            ->setName($data['name'])
            ->setDescription($data['description'])
            ->setDate_added($data['date_added'])
            ->setLogo($data['logo']);
        $this->vendorRepository->save($vendor);
        $this->messageManager->addSuccess(__("Vendor was saved success."));
        $this->_redirect("vendor/vendor/index");
    }
}
