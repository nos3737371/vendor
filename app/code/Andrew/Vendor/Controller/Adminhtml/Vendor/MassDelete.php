<?php

namespace Andrew\Vendor\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Andrew\Vendor\Model\ResourceModel\Vendor\CollectionFactory;
use Andrew\Vendor\Api\VendorRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Andrew_Vendor::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param VendorRepositoryInterface $vendorRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        VendorRepositoryInterface $vendorRepository
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $vendor) {
            $this->vendorRepository->delete($vendor);
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        $this->_redirect("vendor/vendor/index");
    }
}
